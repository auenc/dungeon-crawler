extends Node

const TILE_WIDTH = 70
const TILE_HEIGHT = 120

const MAP_SIZE = 40

onready var tilemap = $Navigation2D/TileMap
onready var nav_2d : Navigation2D = $Navigation2D
onready var character : Sprite = $character
onready var line_2d : Line2D = $Line2D

var map = []

enum Tile{Brick, Grass}

func _ready():
	pass
	#randomize()
	#buildBaseMap()

func _unhandled_input(event) -> void:
	if not event is InputEventMouseButton:
		return
	if event.button_index != BUTTON_LEFT or not event.pressed:
		return
	var tilePosW = tilemap.world_to_map(event.global_position)
	var new_path : = nav_2d.get_simple_path(character.global_position, event.global_position)
	
	var characterPosW = tilemap.world_to_map(character.global_position)
	print("Clicked gPos ", event.global_position, " char gPos ", character.global_position, " clicked wgPos ", tilePosW, " character wgPos ", characterPosW) 
	line_2d.points = new_path
	#character.path = new_path

func buildBaseMap():
	for x in range(MAP_SIZE):
		map.append([])
		for y in range(MAP_SIZE):
			map[x].append(Tile.Grass)
			tilemap.set_cell(x, y, Tile.Grass)
			var tile = tilemap.get_cell(x, y)
			var tileTransform : Transform2D = Transform2D(0.0, Vector2(x, y))
			nav_2d.navpoly_add(tilemap.tile_set.tile_get_navigation_polygon(0), tileTransform)
	
	

#onready var nav : Navigation2D = $Navigation2D

#var path : PoolVector2Array
#var goal : Vector2
#export var speed := 250

#func _input(event: InputEvent):
#	if event is InputEventMouseButton:
#		if event.button_index == BUTTON_LEFT and event.pressed:
#			goal = tilemap.map_to_world(event.position)
#			#print("We clicked on", goal, " and are on ", tilemap.map_to_world($character.position))
#			print("We clicked on", calculatePos(Vector2(event.global_x, event.global_y)), " and are on ", $character.position)
#			path = nav.get_simple_path($character.position, goal, false)
#			$Line2D.points = PoolVector2Array(path)
#			$Line2D.show()
			
#func _process(delta: float) -> void:
#	if !path:
#		$Line2D.hide()
#		return
#	if path.size() > 0:
#		var d: float = $character.position.distance_to(path[0])
		#if d > 10:
			#$character.position = $character.position.linear_interpolate(path[0], (speed * delta)/d)
		#else:
			#path.remove(0)
			
		
#func calculatePos(position: Vector2) -> Vector2:
#	var pos : = Vector2(int(position.x / TILE_WIDTH), int(position.y /  TILE_WIDTH))
#	return pos